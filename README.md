# basht

### A terminal file manager bash script that supports image previews (among other features).

![basht_img.png](screenshots/basht_img.png){width=500}

It is a simple script in bash.This tui file manager provides image preview, theme selection, smooth directory navigation, opening files with default and other programs and easy configuring of keybindings. It uses `fzf` to navigate to and select files and directories. Image rendering can be done with the use of [ueberzugpp](https://github.com/jstkdng/ueberzugpp), [ueberzug](https://github.com/seebye/ueberzug), [kitty terminal](https://sw.kovidgoyal.net/kitty/) or [chafa](https://github.com/hpjansson/chafa/).


![basht_img_2.png](screenshots/basht_img_2.png){width=400}
![basht_img_2.png](screenshots/basht_img_3.png){width=400}

The script also provides content preview for directories, and text files:

![basht_dir.png](screenshots/basht_dir.png){width=400}
![basht_text.png](screenshots/basht_text.png){width=400}

As one can see in the screenshots, thanks to [Nerd Fonts](https://www.nerdfonts.com/font-downloads), each type of selection (directory, text file, office document, image file etc) is represented with the respective symbol (like , , , , , ,  etc).

Many applications are used as default to open various file types, as xed for .txt, viewnior for images, libreoffice for office files etc. However the user can edit the configuration file and  fill in his prefered and already installed applications.

**Basht** is highly configurable, because by tweeking a simple config file, `~/.config/basht/config.psv`, the user can set their **preferred / default applications, preferred shortcuts for various actions, and preferred bookmarks to navigate in the file system.**

---

## DEPENDENCIES

- [fzf](https://github.com/junegunn/fzf), the program which this script is based on.



- [Nerd fonts](https://www.nerdfonts.com/font-downloads), necessary to render the symbols used.

    Just download the `Ubuntu Nerd Font`, unzip and copy the ttf files to `~/.local/share/fonts`.

- [imagemagick](https://imagemagick.org/), [odt2txt](https://github.com/dstosberg/odt2txt/), [catdoc](https://linux.die.net/man/1/catdoc),  [pdftoppm](https://linux.die.net/man/1/pdftoppm), [ffmpegthumbnailer](https://github.com/dirkvdb/ffmpegthumbnailer) for rendering text and image previews.
-  [epub-thumbnailer](https://github.com/marianosimone/epub-thumbnailer) for rendering epub thumbnails.Installation instruction on the repo page.


---
### Installing preferred image support programs

Terminal Image Previews can be achieved with these alternatives:


- [ueberzugpp](https://github.com/jstkdng/ueberzugpp) installation instructions are found on the link.

---

---

- [ueberzug](https://github.com/seebye/ueberzug).  The [ueberzug](https://github.com/seebye/ueberzug) project has been archived. However, in order to install `ueberzug` one can follow these steps:

#### Install `ueberzug` dependencies:

```
sudo apt install libx11-dev libxres-dev libxext-dev

```

- If during the installation process, errors appear due to absence of other depedencies, the user is encouraged to search the error message in the internet in order to locate the misssing dependency.

- Follow the install instructions found in [this ueberzug fork](https://github.com/gokberkgunes/ueberzug-tabbed):

```
  git clone "https://github.com/gokberkgunes/ueberzug-tabbed.git"

  cd ueberzug-tabbed

  python -m pip install .

```

**NOTE**: One may need to call above `pip install` commands as `pip install --break-system-packages` to successfully install the packages.

---

- [kitty terminal](https://sw.kovidgoyal.net/kitty/)

```
sudo apt install kitty
```

---
-  [chafa](https://github.com/hpjansson/chafa/)

```
sudo apt install chafa
```

---
To install the rest of the dependencies:
```
sudo apt install fzf imagemagick odt2txt catdoc pdftoppm ffmpegthumbnailer
```

To install `epub-thumbnailer` (the program to extract thumbnail from an `.epub` file), follow the instructions here:
[https://github.com/marianosimone/epub-thumbnailer](https://github.com/marianosimone/epub-thumbnailer)

---

## INSTALL-RUN

From a terminal, clone the repo:

```
git clone https://gitlab.com/christosangel/basht.git
```


Then, navigate to the `basht/` directory:

```
cd basht/
```
Make `install.sh` executable:

```
chmod +x install.sh
```
And just run it:

```
./install.sh
```

Run `basht.sh` from any directory:

```
basht.sh
```
---
## USAGE

###  CHANGE THE THEME

Using the `ctrl-t` keybinding, you can choose the theme of your liking, among many:

![theme_nor.png](screenshots/theme_nor.png){width=400}
![theme_sol.png](screenshots/theme_sol.png){width=400}
![theme_ubu.png](screenshots/theme_ubu.png){width=400}
![theme_nor.esm](screenshots/theme_esm.png){width=400}

Most of these themes can be found [here](https://github.com/junegunn/fzf/wiki/Color-schemes).
If the user feels like adjusting the colors, or even add their own custom themes, they can do so by editing the `~/.config/basht/themes.txt` file.
To this end, the user can use this [theme generator](https://vitormv.github.io/fzf-themes/)

---

### KEY BINDINGS

The script is highly costumizable as far the keyboard bindings, the shortcuts and the applications that can be used.
The user can view this script's shortcuts, using `alt-s` from the keyboard. The presented shortcut list is parsed from the `~/.config/basht/config.psv` file. By editing the same file, the user can configure these keybindings to their liking. This is done by changing the elements in the  **Shortcuts column**, the fourth from the left (ie ctrl-y instead of ctrl-c, etc).

|Action|Config Variable|Shortcut|
|---|---|---|
|Copy  |\_COPY\_|ctrl-c
|Paste  |\_PASTE\_|ctrl-v
|Cut  |\_CUT\_|ctrl-x
|Rename  |\_RENAME\_|ctrl-r
|Move to Trash  |\_DELETE\_|ctrl-d
|Create new file  |\_CREATE-N-FILE\_|ctrl-n
|Create new dir  |\_CREATE-N-DIR\_|ctrl-alt-n
|Open with  |\_OPEN-WITH\_|ctrl-o
|Open with (all)  |\_SELECT-APP\_|ctrl-alt-o
|Select Theme  |\_THEME\_|ctrl-t
|Quit Basht|\_QUIT\_|ctrl-q
|Edit Preferences  |\_CONF\_|ctrl-p
|Show Preferences  |\_PREFERENCES\_|alt-p
|View History  |\_HISTORY\_|alt-h
|Show Shortcuts  |\_SHORT\_|alt-s
|Show Bookmarks  |\_BOOK\_|alt-b
|Empty Trash  |\_EMPTY-TRASH\_|ctrl-z
|Empty Cache  |\_EMPTY-CACHE\_|alt-m
|Open  terminal  |\_OPENTERMINAL\_|alt-t

![shortcuts.png](screenshots/shortcuts.png){width=300}

- Using `ctrl-o` for instance on a *text file*, the user can select one of the **preconfigured text editors** to open.

-  By using the `ctrl-alt-o` keybinding on the other hand, the user can chose from a wider range of applications, not only text editors.

![open_1.png](screenshots/open_1.png){width=200}
![open_2.png](screenshots/open_2.png){width=200}

- Using the `ctrl-d` keybinding on a file, will in fact, not delete it, but move it o the `Trash` file. As anextra precaution, the user will be prompted before each such deletion. This prompt can be deleted in the code, by commenting(#) a few marked lines in the `basht.sh` file, if the user so chooses.

![delete_prompt.png](screenshots/delete_prompt.png){width=400}

---


###  BOOKMARKS

The key combinations `alt-[0-9,z,x,c,v]` are used as bookmarks to various locations.
These Bookmarks again are quite arbitrary, the user can alter them to their preference editing `~/.config/basht/config.psv `.

| Directory    |  Keybinding |
|-----|-----|
|Parent directory  |left|
|Previous directory|alt-left|
|/home/username/ |alt-0|
|/home/username/  |alt-1|
|/home/username/Desktop  |alt-2|
|/home/username/Documents/ |alt-3|
|/home/username/Downloads/  |alt-4|
|/home/username/Music/  |alt-5|
|/home/username/Pictures/  |alt-6|
|/home/username/Videos/ |alt-7|
|/home/username/  |alt-8|
|/home/username/ |alt-9|
|/home/username/.local/share/Trash/files/  |alt-z|
|/home/username/  |alt-x|
|/home/username/  |alt-c|
|/home/username/  |alt-v|

![bookmarks.png](screenshots/bookmarks.png){width=350}

---
###  PREFERRED APPLICATIONS

Obviously the applications included in the `~/.config/config.psv` file are the applications that work for me, like _audacity, calibre, retext, vim, nano, xed, inkscape, gaupol_ etc.

The user  is of course **encouraged to edit the     `~/.config/config.psv` file, and add their preferred text editors, media playersand other programs**.

|Description|Config Variable|Preferred Program|
|---|---|---|
|Default text editor: |\_DEFAULTTEXT\_APP\_|nano -ml|
|Text editor 1: |\_TEXT\_APP1\_|nano -ml|
|Text editor 2: |\_TEXT\_APP2\_|xed|
|Text editor 3: |\_TEXT\_APP3\_|vim|
|Execute shell: |\_TEXT\_APP4\_|bash|
|Markdown editor : |\_DEFAULT\_MD\_|retext|
|Default audio player: |\_DEFAUDIO\_APP\_|mpv|
|Default video player: |\_DEFVIDEO\_APP\_|mpv|
|Media player 1: |\_MEDIA\_APP1\_|mpv|
|Media player 2: |\_MEDIA\_APP2\_|celluloid|
|Media player 3: |\_MEDIA\_APP3\_|vlc|
|Media player 4: |\_MEDIA\_APP4\_|play|
|Audio editor: |\_MEDIA\_APP5\_|audacity|
|Default image viewer: |\_DEFIMAGE\_APP\_|viewnior|
|Image viewer 1: |\_IMAGE\_APP1\_|viewnior|
|Graphics program 1: |\_IMAGE\_APP2\_|gimp|
|Graphics program 2: |\_IMAGE\_APP3\_|inkscape|
|Subs editor: |\_SUB\_EDITOR\_|gaupol|
|Office suite: |\_DEF\_OFFICE\_|libreoffice|
|Pdf Reader: |\_DEF\_PDF\_|xreader|
|E-book Reader: |\_DEF\_EPUB\_|ebook-viewer --detach|
|Default web browser: |\_DBROWSER\_|brave-browser|
|Web Browser 1: |\_BROWSER1\_|firefox|
|Web Browser 2: |\_BROWSER2\_|brave-browser|
|Text Editor: |\_BROWSER3\_|xed|
|Terminal: |\_PREFTERM\_|xfce4-terminal|

![preferences.png](screenshots/preferences.png){width=300}

---

### CONFIGURE

- As mentioned already, cofiguring can be done by editing the `~/.config/config.psv` file, easily, using the `ctrl-p` keybinding, whch will open the file on the preferred browser.

- By editing this line, the user can configure the way images are rendered in the preview:

```
CONFIG   |Image Support(kitty, ueberzug, chafa, none)|IMAGE_SUPPORT|ueberzugpp
```

- Adding more lines to this file, with i.e. another media player, should be done like this (if we want to add let's say *xplayer*:

```
APPLICATION |Media player 5: |_MEDIA_APP5_|xplayer
```
This way, `xplayer` will show up everytime the uses hits `ctrl-o` on a media file.

The user can add **as many players as  they like**, provided the same format is kept:

```
APPLICATION |Media player 5: |_MEDIA_APP5_|xplayer
APPLICATION |Media player 6: |_MEDIA_APP5_|wplayer
APPLICATION |Media player 7: |_MEDIA_APP5_|any
APPLICATION |Media player 8: |_MEDIA_APP5_|other
APPLICATION |Media player 9: |_MEDIA_APP5_|player
```

The same applies of course to other type of applications (text editors, graphics programs, audio, etc).

- If the user wants to have a program, again for instance `xplayer ` as the default video player:

```
APPLICATION |Default video player: |_DEFVIDEO_APP_|xplayer
```
---
### LOGGED HISTORY

Using the `alt-h `, the user can browse the basht  `log file`.

The full path to the log file: `$HOME/.cache/basht/log.txt `

---
I hope you find this project useful, any feedback/suggestions will be appreciated.

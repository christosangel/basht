#! /bin/bash
#make script executable, copy it to the $PATH
chmod +x basht.sh
cp basht.sh $HOME/.local/bin/
#create dirs and files
mkdir -p $HOME/.cache/basht/thumbnails $HOME/.config/basht/
touch $HOME/.config/basht/themes.txt $HOME/.config/basht/config.psv $HOME/.config/basht/current_theme.txt
cp basht.png $HOME/.cache/basht/basht.png
themes="Exit
None
Nordisk  --color=fg:#D8DEE9,bg:#2E3440,hl:#6c6c6c,fg+:#D8DEE9,bg+:#434C5E,hl+:#A3BE8C,pointer:#BF616A,info:#4C566A,spinner:#4C566A,header:#DCD81F,prompt:#81A1C1,marker:#EBCB8B
Molokai  --color=fg:#d0d0d0,bg:#121212,hl:#5f87af,fg+:#d0d0d0,bg+:#262626,hl+:#5fd7ff,info:#afaf87,prompt:#d7005f,spinner:#af5fff,pointer:#af5fff,marker:118
Oblivious  --color=fg:#d0d0d0,bg:#2E3436,hl:#5f87af,fg+:#d0d0d0,bg+:#555753,hl+:#5fd7ff,info:#afaf87,prompt:#BF616A,spinner:#af5fff,pointer:#BF616A,header:#DCD81F,marker:#EBCB8B
Seoul_256_Night  --color=fg:#6c6c6c,bg:#121212,hl:65,fg+:15,bg+:234,hl+:108,info:108,prompt:109,spinner:108,pointer:168,marker:168
Red_Dark  --color=fg:#af0000,bg:#000000,hl:202,fg+:214,bg+:52,hl+:231,info:52,prompt:196,spinner:208,pointer:196,marker:208
Solarized_Colors  --color=bg+:#073642,bg:#002b36,spinner:#719e07,hl:#586e75,fg:#839496,header:#586e75,info:#cb4b16,pointer:#719e07,marker:#719e07,fg+:#839496,prompt:#719e07,hl+:#719e07
Jellybeans  --color=fg:#d7d7d7,bg:#121212,hl:103,fg+:222,bg+:234,hl+:104,info:183,prompt:110,spinner:107,pointer:167,marker:215
Seoul_Dusk  --color=fg:#6c6c6c,bg:#303030,hl:65,fg+:15,bg+:239,hl+:108,info:108,prompt:109,spinner:108,pointer:168,marker:168
One_Dark  --color=fg:#cbccc6,bg:#1f2430,hl:#bd93f9,fg+:#f8f8f2,bg+:#191e2a,hl+:#ffcc66,info:#73d0ff,prompt:#50fa7b,pointer:#73d0ff,marker:#73d0ff,spinner:#73d0ff,header:#73d0ff
Gruvbox_Dark  --color=fg:#ebdbb2,bg:#282828,hl:#fabd2f,fg+:#ebdbb2,bg+:#3c3836,hl+:#fabd2f,info:#83a598,prompt:#bdae93,spinner:#fabd2f,pointer:#83a598,marker:#fe8019,header:#665c54
Spacecamp  --color=fg:#dedede,bg:#121212,hl:#666666,fg+:#eeeeee,bg+:#282828,hl+:#cf73e6,info:#cf73e6,prompt:#FF0000,pointer:#cf73e6,marker:#f0d50c,spinner:#cf73e6,header:#91aadf
Term_School  --color=fg:#f0f0f0,bg:#252c31,bg+:#005f5f,hl:#87d75f,gutter:#252c31,prompt:#f0f0f0,pointer:#dfaf00,marker:#00d7d7
Dracula  --color=fg:#f8f8f2,bg:#222d31,hl:#bd93f9,fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9,info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6,marker:#ff79c6,spinner:#ffb86c,header:#6272a4
Ubunto  --color=fg:#F1F707,bg:#360B28,hl:#666666,fg+:#eeeeee,bg+:#621342,hl+:#cf73e6,info:#cf73e6,prompt:#FF0000,pointer:#cf73e6,marker:#f0d50c,spinner:#cf73e6,header:#91aadf
Esmeralda  --color=fg:#50fa7b,bg:#152C23,hl:#23FA0B,fg+:#23FA0B,bg+:#3E4541,hl+:#23FA0B,pointer:#BF616A,info:#BE998F,spinner:#BE998F,header:#DCD81F,prompt:#B4AE10,marker:#BF616A
Dracullyrian  --color=fg:#D8E6D0,bg:#222d31,hl:#bd93f9,fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9,info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6,marker:#ff79c6,spinner:#ffb86c,header:#6272a4"
config="CONFIG   |Image Support(kitty, ueberzugpp, ueberzug, chafa, none)|IMAGE_SUPPORT|ueberzug
SHORTCUT |Copy : |_COPY_|ctrl-c
SHORTCUT |Paste : |_PASTE_|ctrl-v
SHORTCUT |Cut : |_CUT_|ctrl-x
SHORTCUT |Rename : |_RENAME_|ctrl-r
SHORTCUT |Move to Trash : |_DELETE_|ctrl-d
SHORTCUT |Create new file : |_CREATE-N-FILE_|ctrl-n
SHORTCUT |Create new dir : |_CREATE-N-DIR_|ctrl-alt-n
SHORTCUT |Open with : |_OPEN-WITH_|ctrl-o
SHORTCUT |Open with (all) : |_SELECT-APP_|ctrl-alt-o
SHORTCUT |Select Theme : |_THEME_|ctrl-t
SHORTCUT |Quit Basht : |_QUIT_|ctrl-q
SHORTCUT |Edit Preferences : |_CONF_|ctrl-p
SHORTCUT |Show Preferences : |_PREFERENCES_|alt-p
SHORTCUT |View History : |_HISTORY_|alt-h
SHORTCUT |Show Shortcuts : |_SHORT_|alt-s
SHORTCUT |Show Bookmarks : |_BOOK_|alt-b
SHORTCUT |Empty Trash : |_EMPTY-TRASH_|ctrl-z
SHORTCUT |Empty Cache : |_EMPTY-CACHE_|alt-m
SHORTCUT |Open  terminal : |_OPENTERMINAL_|alt-t
BOOKMARK |Parent directory : |_PARENT_|left
BOOKMARK |Previous Directory: |_PREVIOUS-DIR_|alt-left
BOOKMARK |$HOME/ : |_alt_0_|alt-0
BOOKMARK |$HOME/ : |_alt_1_|alt-1
BOOKMARK |$HOME/Desktop : |_alt_2_|alt-2
BOOKMARK |$HOME/Documents/ : |_alt_3_|alt-3
BOOKMARK |$HOME/Downloads/ : |_alt_4_|alt-4
BOOKMARK |$HOME/Music/ : |_alt_5_|alt-5
BOOKMARK |$HOME/Pictures/ : |_alt_6_|alt-6
BOOKMARK |$HOME/Videos/ : |_alt_7_|alt-7
BOOKMARK |$HOME/ : |_alt_8_|alt-8
BOOKMARK |$HOME/ : |_alt_9_|alt-9
BOOKMARK |$HOME/.local/share/Trash/files/ : |_alt_z_|alt-z
BOOKMARK |$HOME/ : |_alt_x_|alt-x
BOOKMARK |$HOME/ : |_alt_c_|alt-c
BOOKMARK |$HOME/ : |_alt_v_|alt-v
APPLICATION |Default text editor: |_DEFAULTTEXT_APP_|nano -ml
APPLICATION |Text editor 1: |_TEXT_APP1_|nano -ml
APPLICATION |Text editor 2: |_TEXT_APP2_|xed
APPLICATION |Text editor 3: |_TEXT_APP3_|vim
APPLICATION |Execute shell: |_TEXT_APP4_|bash
APPLICATION |Markdown editor : |_DEFAULT_MD_|retext
APPLICATION |Default audio player: |_DEFAUDIO_APP_|mpv
APPLICATION |Default video player: |_DEFVIDEO_APP_|mpv
APPLICATION |Media player 1: |_MEDIA_APP1_|mpv
APPLICATION |Media player 2: |_MEDIA_APP2_|celluloid
APPLICATION |Media player 3: |_MEDIA_APP3_|vlc
APPLICATION |Media player 4: |_MEDIA_APP4_|play
APPLICATION |Audio editor: |_MEDIA_APP5_|audacity
APPLICATION |Default image viewer: |_DEFIMAGE_APP_|viewnior
APPLICATION |Image viewer 1: |_IMAGE_APP1_|viewnior
APPLICATION |Graphics program 1: |_IMAGE_APP2_|gimp
APPLICATION |Graphics program 2: |_IMAGE_APP3_|inkscape
APPLICATION |Subs editor: |_SUB_EDITOR_|gaupol
APPLICATION |Office suite: |_DEF_OFFICE_|libreoffice
APPLICATION |Pdf Reader: |_DEF_PDF_|xreader
APPLICATION |E-book Reader: |_DEF_EPUB_|ebook-viewer --detach
APPLICATION |Default web browser: |_DBROWSER_|brave-browser
APPLICATION |Web Browser 1: |_BROWSER1_|firefox
APPLICATION |Web Browser 2: |_BROWSER2_|brave-browser
APPLICATION |Text Editor: |_BROWSER3_|xed
APPLICATION |Terminal: |_PREFTERM_|xfce4-terminal"
echo "$themes">$HOME/.config/basht/themes.txt
echo "$config">$HOME/.config/basht/config.psv
